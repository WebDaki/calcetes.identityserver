﻿using BECore.Application.Context;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Entities;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Repositories;
using Calcetes.IdentityServer.Domain.Aggregates.UserSessions.Entities;
using Calcetes.IdentityServer.Domain.Aggregates.UserSessions.Repositories;
using Calcetes.IdentityServer.Domain.Identification;
using Calcetes.IdentityServer.Domain.Services;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading.Tasks;

namespace Calcetes.IdentityServer.Api.Filters
{
    internal class LoggedAttribute : ActionFilterAttribute
    {
        private const string CALCETES_IDENTITY_KEY = "CALCETES_IDENTITY";

        public async Task OnActionExecutingAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            OnActionExecuting(context);
            await next();
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var token = GetToken();

            var session = GetSession(token);

            var user = GetUser(session);

            SetUserInContext(context, user);
        }

        private string GetToken()
        {
            var _cookiesService = (ICookiesService)ApplicationContext.Provider.GetService(typeof(ICookiesService));

            var token = _cookiesService.Get(CALCETES_IDENTITY_KEY);

            if (_cookiesService is IDisposable disposable)
                disposable.Dispose();

            if (token == null)
                throw new UnauthorizedAccessException("Token not fount");

            return token;
        }

        private UserSession GetSession(string token)
        {
            var _sesionsRepo = (IUserSessionsRepository)ApplicationContext.Provider.GetService(typeof(IUserSessionsRepository));

            var session = _sesionsRepo.ByToken(token);

            if (_sesionsRepo is IDisposable disposable)
                disposable.Dispose();

            if (session == null)
                throw new UnauthorizedAccessException("Session not fount");

            return session;
        }

        private User GetUser(UserSession session)
        {
            var _usersRepo = (IUsersMemory)ApplicationContext.Provider.GetService(typeof(IUsersMemory));

            var user = _usersRepo.Find(session.UserId);

            if (_usersRepo is IDisposable disposable)
                disposable.Dispose();

            if (user == null)
                throw new UnauthorizedAccessException("User not fount");

            return user;
        }

        private void SetUserInContext(ActionExecutingContext context, User user)
        {
            context.HttpContext.User = new GenericPrincipal(new ContextUser(user.Id, user.Name), new List<string>().ToArray());
        }
    }
}