﻿using BECore.Application.Dispatchers;
using Calcetes.IdentityServer.Application.Aggregates.UserSessions.Dtos.Queries;
using Calcetes.IdentityServer.Application.Aggregates.UserSessions.Dtos.Dtos;
using Microsoft.AspNetCore.Mvc;
using BECore.Application.Message;

namespace Calcetes.IdentityServer.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserSessionsController : ControllerBase
    {
        private readonly IQueryDispatcher _queryDispatcher;
        private readonly ICommandDispatcher _commandDispatcher;
        public UserSessionsController(IQueryDispatcher queryDispatcher, ICommandDispatcher commandDispatcher)
        {
            _queryDispatcher = queryDispatcher;
            _commandDispatcher = commandDispatcher;
        }

        [HttpGet]
        public ActionResult<TokenDto> Get(string userToken)
        {
            return Ok(_queryDispatcher.Ask<RefreshUserTokenQuery, QueryResult<TokenDto>>(new RefreshUserTokenQuery { Token = userToken }).Result);
        }

        [HttpPost]
        public ActionResult<TokenDto> Post([FromBody] AuthenticateUserQuery query)
        {
            return Ok(_queryDispatcher.Ask<AuthenticateUserQuery, QueryResult<TokenDto>>(query).Result);
        }
    }
}
