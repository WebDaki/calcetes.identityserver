﻿using BECore.Application.Dispatchers;
using BECore.Application.Message;
using Calcetes.IdentityServer.Api.Filters;
using Calcetes.IdentityServer.Application.Aggregates.Users.Dtos.Commands;
using Calcetes.IdentityServer.Application.Aggregates.Users.Dtos.Dtos;
using Calcetes.IdentityServer.Application.Aggregates.Users.Dtos.Queries;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Calcetes.IdentityServer.Api.Controllers
{
    [Logged]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IQueryDispatcher _queryDispatcher;
        private readonly ICommandDispatcher _commandDispatcher;
        public UsersController(IQueryDispatcher queryDispatcher, ICommandDispatcher commandDispatcher)
        {
            _queryDispatcher = queryDispatcher;
            _commandDispatcher = commandDispatcher;
        }

        [HttpGet("CurrentUser")]
        public ActionResult<UserDto> CurrentUser()
        {
            return Ok(_queryDispatcher.Ask<MeQuery, QueryResult<UserDto>>(new MeQuery()).Result);
        }

        [HttpGet]
        public ActionResult<UserDto> Get(string userToken)
        {
            return Ok(_queryDispatcher.Ask<ByTokenQuery, QueryResult<UserDto>>(new ByTokenQuery { Token = userToken }).Result);
        }

        [HttpGet("ById")]
        public ActionResult<UserDto> ById(Guid userId)
        {
            return Ok(_queryDispatcher.Ask<ByUserIdQuery, QueryResult<UserDto>>(new ByUserIdQuery { UserId = userId }).Result);
        }

        [HttpPost]
        public ActionResult Post([FromBody] CreateUserCommand command)
        {
            _commandDispatcher.Dispatch(command);
            return Ok();
        }
    }
}
