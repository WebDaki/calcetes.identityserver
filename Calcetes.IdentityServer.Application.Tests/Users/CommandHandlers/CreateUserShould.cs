using BECore.Application.Hashers;
using BECore.Domain.Events;
using BECore.Domain.Exceptions;
using Calcetes.IdentityServer.Application.Aggregates.Users.Dtos.Commands;
using Calcetes.IdentityServer.Application.Aggregates.Users.Handlers.CommandHandlers;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Entities;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Events;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Repositories;
using Calcetes.IdentityServer.Domain.Base.Exceptions;
using Calcetes.IdentityServer.Domain.Enumerations;
using Calcetes.IdentityServer.Domain.Services;
using Moq;
using NUnit.Framework;
using System;

namespace Calcetes.IdentityServer.Application.Tests.Users.CommandHandlers
{
    public class CreateUserShould
    {
        private Mock<IUsersRepository> usersRepoMock;
        private Mock<IDomainService> domain;
        private Mock<IPasswordHasher> hasherMock;
        private Mock<IEventSender> eventSenderMock;
        private Mock<IContext> contextMock;
        private CreateUserCommandHandler handler;

        private static readonly string loggedToken = "LoggedToken";
        private static readonly Guid loggedId = Guid.NewGuid();
        private static readonly string loggedName = "LoggedName";
        private static readonly string loggedMail = "logged@mail.es";
        private static readonly string loggedPass = "passPASS1234";
        private static readonly UserType loggedType = UserType.Resource;
        private static readonly User loggedUser = new User
        {
            Id = loggedId,
            Name = loggedName,
            Pass = loggedPass,
            Mail = loggedMail,
            Type = loggedType,
            IsVip = true,
            Version = 1,
            CreatorId = Guid.NewGuid(),
            Created = DateTime.UtcNow,
            UpdatorId = Guid.NewGuid(),
            Updated = DateTime.UtcNow
        };

        private static readonly Guid userId = Guid.NewGuid();
        private static readonly string userName = "UserName";
        private static readonly string userMail = "user@mail.es";
        private static readonly string userPass = "passPASS1234";
        private static readonly UserType userType = UserType.Admin;
        private static readonly User existingUser = new User {
            Id = userId,
            Name = userName,
            Pass = userPass,
            Mail = userMail,
            Type = userType,
            IsVip = true,
            Version = 1,
            CreatorId = Guid.NewGuid(),
            Created = DateTime.UtcNow,
            UpdatorId = Guid.NewGuid(),
            Updated = DateTime.UtcNow
        };

        private static readonly Guid commandId = Guid.NewGuid();
        private static readonly string commandName = "CommandName";
        private static readonly string commandMail = "command@mail.es";
        private static readonly string commandPass = "passPASS1234";
        private static readonly UserType commandType = UserType.Resource;
        private static readonly User newUser = new User
        {
            Id = commandId,
            Name = commandName,
            Pass = commandPass,
            Mail = commandMail,
            Type = commandType,
            IsVip = true,
            Version = 1,
            CreatorId = Guid.NewGuid(),
            Created = DateTime.UtcNow,
            UpdatorId = Guid.NewGuid(),
            Updated = DateTime.UtcNow
        };

        [SetUp]
        public void Setup()
        {
            usersRepoMock = GetUsersRepositoryMock();
            domain = GetDomainServiceMock();
            hasherMock = new Mock<IPasswordHasher>();
            eventSenderMock = new Mock<IEventSender>();
            contextMock = GetContextMock();
            handler = new CreateUserCommandHandler(usersRepoMock.Object, domain.Object, hasherMock.Object, eventSenderMock.Object, contextMock.Object);
        }
        private Mock<IUsersRepository> GetUsersRepositoryMock()
        {
            var mock = new Mock<IUsersRepository>();

            mock
                .Setup(repo => repo.ById(userId))
                .Returns(existingUser);

            mock
                .Setup(repo => repo.ById(loggedId))
                .Returns(loggedUser);

            mock
                .Setup(repo => repo.FindByNameType(userName, userType))
                .Returns(existingUser);

            return mock;
        }

        private Mock<IDomainService> GetDomainServiceMock()
        {
            var mock = new Mock<IDomainService>();

            mock
                .Setup(service => service.CreateUser(commandId, commandName, commandMail, It.IsNotIn<string>(commandPass), It.IsAny<bool>(), commandType, loggedId))
                .Returns(newUser);

            return mock;
        }

        private Mock<IContext> GetContextMock()
        {
            var mock = new Mock<IContext>();

            mock
                .Setup(context => context.GetCurrentApplicationUser())
                .Returns(new Domain.Identification.ContextUser(loggedId, loggedName));

            return mock;
        }
        
        [Test]
        public void Validate_NotNullCommand()
        {
            TestDelegate runNullCommand = (() => handler.Execute(NullCommand()));

            ThowsWithoutModify<NullCommandException>(runNullCommand);
        }

        [Test]
        public void Validate_UserId()
        {
            usersRepoMock
                .Setup(repo => repo.ById(userId))
                .Returns(existingUser);

            TestDelegate runNoIdCommand = (() => handler.Execute(NoIdCommand()));
            TestDelegate runIdInUseCommand = (() => handler.Execute(IdInUseCommand()));

            ThowsWithoutModify<UserIdRequiredException>(runNoIdCommand);
            ThowsWithoutModify<UserIdInUseException>(runIdInUseCommand);
        }

        [Test]
        public void Validate_UserType()
        {
            TestDelegate runNoTypeCommand = (() => handler.Execute(NoTypeCommand()));
            TestDelegate runWrongCommandCommand = (() => handler.Execute(wrongTypeCommand()));

            ThowsWithoutModify<UserTypeRequiredException>(runNoTypeCommand);
            ThowsWithoutModify<UserTypeBatFormatException>(runWrongCommandCommand);
        }

        [Test]
        public void Validate_UserName()
        {
            TestDelegate runNoNameCommand = (() => handler.Execute(NoNameCommand()));
            TestDelegate runNameInUseCommand = (() => handler.Execute(NameInUseCommand()));

            ThowsWithoutModify<UserNameRequiredException>(runNoNameCommand);
            ThowsWithoutModify<UserNameInUseException>(runNameInUseCommand);
        }

        [Test]
        public void Validate_UserMail()
        {
            TestDelegate runNoMailCommand = (() => handler.Execute(NoMailCommand()));
            TestDelegate runBatMailCommand = (() => handler.Execute(BadMailCommand()));

            ThowsWithoutModify<UserMailRequiredException>(runNoMailCommand);
            ThowsWithoutModify<UserMailBatFormatException>(runBatMailCommand);
        }

        [Test]
        public void Validate_UserPass()
        {
            TestDelegate runNoPassCommand = (() => handler.Execute(NoPassCommand()));
            TestDelegate runNoUpperPassCommand = (() => handler.Execute(NoUpperPassCommand()));
            TestDelegate runNoLowerPassCommand = (() => handler.Execute(NoLowerPassCommand()));
            TestDelegate runNoNumberCommand = (() => handler.Execute(NoNumberPassCommand()));
            TestDelegate runShortPassCommand = (() => handler.Execute(ShortPassCommand()));

            ThowsWithoutModify<UserPassRequiredException>(runNoPassCommand);
            ThowsWithoutModify<UserPassBatFormatException>(runNoUpperPassCommand);
            ThowsWithoutModify<UserPassBatFormatException>(runNoLowerPassCommand);
            ThowsWithoutModify<UserPassBatFormatException>(runNoNumberCommand);
            ThowsWithoutModify<UserPassBatFormatException>(runShortPassCommand);
        }

        [Test]
        public void Correct_Command_Creates_User_And_Send_Message()
        {
            handler.Execute(CorrectCommand());
            domain.Verify(service => 
                service.CreateUser(
                    /* id */        It.IsAny<Guid>(),
                    /* name */      It.IsAny<string>(),
                    /* mail */      It.IsAny<string>(),
                    /* pass */      It.IsNotIn<string>(commandPass),
                    /* isVip */     It.IsAny<bool>(),
                    /* type */      It.IsAny<UserType>(),
                    /* creatorId */ It.IsAny<Guid>()
                ),
                Times.Once
            );
            usersRepoMock.Verify(repo => repo.Add(It.IsAny<User>()), Times.Once);
            usersRepoMock.Verify(repo => repo.Modify(It.IsAny<User>()), Times.Never);
            usersRepoMock.Verify(repo => repo.Delete(It.IsAny<Guid>()), Times.Never);
            eventSenderMock.Verify(sender => sender.Send(It.IsAny<UserCreated>()), Times.Once);
        }

        private void ThowsWithoutModify<TException>(TestDelegate action) where TException : DomainException
        {
            Assert.Throws<TException>(action);
            usersRepoMock.Verify(repo => repo.Add(It.IsAny<User>()), Times.Never);
            usersRepoMock.Verify(repo => repo.Modify(It.IsAny<User>()), Times.Never);
            usersRepoMock.Verify(repo => repo.Delete(It.IsAny<Guid>()), Times.Never);
        }

        private CreateUserCommand NullCommand()
        {
            return null;
        }

        private CreateUserCommand NoIdCommand()
        {
            var command = CorrectCommand();
            command.Id = Guid.Empty;
            return command;
        }

        private CreateUserCommand IdInUseCommand()
        {
            var command = CorrectCommand();
            command.Id = userId;
            return command;
        }

        private CreateUserCommand NoTypeCommand()
        {
            var command = CorrectCommand();
            command.Type = null;
            return command;
        }

        private CreateUserCommand wrongTypeCommand()
        {
            var command = CorrectCommand();
            command.Type = "WRONG_TYPE";
            return command;
        }

        private CreateUserCommand NoNameCommand()
        {
            var command = CorrectCommand();
            command.Name = null;
            return command;
        }

        private CreateUserCommand NameInUseCommand()
        {
            var command = CorrectCommand();
            command.Name = userName;
            command.Type = userType.ToString();
            return command;
        }

        private CreateUserCommand NoMailCommand()
        {
            var command = CorrectCommand();
            command.Mail = null;
            return command;
        }

        private CreateUserCommand BadMailCommand()
        {
            var command = CorrectCommand();
            command.Mail = "NOT_EMAIL_STRING";
            return command;
        }

        private CreateUserCommand NoPassCommand()
        {
            var command = CorrectCommand();
            command.Pass = null;
            return command;
        }

        private CreateUserCommand NoUpperPassCommand()
        {
            var command = CorrectCommand();
            command.Pass = "password12345678";
            return command;
        }

        private CreateUserCommand NoLowerPassCommand()
        {
            var command = CorrectCommand();
            command.Pass = "PASSWORD12345678";
            return command;
        }

        private CreateUserCommand NoNumberPassCommand()
        {
            var command = CorrectCommand();
            command.Pass = "passwordPASSWORD";
            return command;
        }

        private CreateUserCommand ShortPassCommand()
        {
            var command = CorrectCommand();
            command.Pass = "pasPAS123";
            return command;
        }

        private CreateUserCommand CorrectCommand()
        {
            return new CreateUserCommand {
                Id = commandId,
                Name = commandName,
                Mail = commandMail,
                Pass = commandPass,
                IsVip = false,
                Type = commandType.ToString()
            };
        }
    }
}