﻿//using BECore.Application.Dispatchers;
//using BECore.Application.Handlers;
//using BECore.Application.Message;
//using BECore.Domain.Automaticals;
//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace Calcetes.IdentityServer.Application
//{
//    public class Class1 : ICommandDispatcher
//    {
//        private readonly IServiceProvider _provider;
//        public Class1(IServiceProvider provider)
//        {
//            _provider = provider;
//        }

//        public void Dispatch<TCommand>(TCommand command) where TCommand : ICommand
//        {
//            ICommandHandler<TCommand> handler = null;

//            try
//            {
//                handler = GetHandler<TCommand>();
//                handler.Execute(command);
//            }
//            catch (Exception e)
//            {
//                throw e;
//            }
//            finally
//            {
//                if (handler != null)
//                    handler.Dispose();
//            }
//        }

//        private ICommandHandler<TCommand> GetHandler<TCommand>() where TCommand : ICommand
//        {
//            try
//            {
//                return (ICommandHandler<TCommand>)_provider.GetService(typeof(ICommandHandler<TCommand>));
//            }
//            catch (Exception e)
//            {
//                throw new Exception($"CommandHandler not registered for '{ typeof(TCommand).Name }' Command.", e);
//            }
//        }
//    }
//}
