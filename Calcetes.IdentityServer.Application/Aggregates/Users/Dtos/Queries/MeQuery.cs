﻿using BECore.Application.Message;

namespace Calcetes.IdentityServer.Application.Aggregates.Users.Dtos.Queries
{
    public class MeQuery : Query { }
}
