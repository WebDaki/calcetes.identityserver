﻿using BECore.Application.Message;
using System;

namespace Calcetes.IdentityServer.Application.Aggregates.Users.Dtos.Queries
{
    public class ByUserIdQuery : Query
    {
        public Guid UserId { get; set; }
    }
}
