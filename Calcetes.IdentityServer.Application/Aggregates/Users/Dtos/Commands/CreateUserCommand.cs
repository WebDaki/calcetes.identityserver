﻿using BECore.Application.Message;
using Calcetes.IdentityServer.Application.Aggregates.Users.Dtos.Dtos;
using System;

namespace Calcetes.IdentityServer.Application.Aggregates.Users.Dtos.Commands
{
    public class CreateUserCommand : UserDto, ICommand { }
}
