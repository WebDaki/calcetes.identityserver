﻿using Calcetes.IdentityServer.Application.Base;

namespace Calcetes.IdentityServer.Application.Aggregates.Users.Dtos.Dtos
{
    public class UserDto : EntityDto
    {
        public string Name { get; set; }
        public string Mail { get; set; }
        public string Pass { get; set; }
        public string Type { get; set; }
        public bool IsVip { get; set; }
    }
}
