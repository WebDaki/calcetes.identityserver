﻿using Calcetes.IdentityServer.Application.Aggregates.Users.Dtos.Dtos;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Calcetes.IdentityServer.Application.Aggregates.Users.Converters
{
    public static class UserConverter
    {
        public static UserDto ToDto(User user)
        {
            return new UserDto
            {
                Id = user.Id,
                Name = user.Name,
                Mail = user.Mail,
                Type = user.Type.ToString(),
                IsVip = user.IsVip,
                Created = user.Created,
                Creator = user.CreatorId,
                Updated = user.Updated,
                Updator = user.UpdatorId
            };
        }
    }
}
