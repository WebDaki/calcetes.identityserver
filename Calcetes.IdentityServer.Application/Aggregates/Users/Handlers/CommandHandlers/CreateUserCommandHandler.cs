﻿using BECore.Application.Automaticals;
using BECore.Application.Disposable;
using BECore.Application.Handlers;
using BECore.Application.Hashers;
using BECore.Domain.Events;
using Calcetes.IdentityServer.Application.Aggregates.Users.Dtos.Commands;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Entities;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Events;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Repositories;
using Calcetes.IdentityServer.Domain.Base.Exceptions;
using Calcetes.IdentityServer.Domain.Enumerations;
using Calcetes.IdentityServer.Domain.Services;
using System;
using System.Linq;

namespace Calcetes.IdentityServer.Application.Aggregates.Users.Handlers.CommandHandlers
{
    public class CreateUserCommandHandler : AutoDisposable, ICommandHandler<CreateUserCommand>, IInjectable
    {
        private const string EmailAllowedCharacters = "abcdefghijklmnopqrstuvwxyz@.";
        private readonly IUsersRepository _usersRepo;
        private readonly IDomainService _domain;
        private readonly IPasswordHasher _hasher;
        private readonly IEventSender _eventSender;
        private readonly IContext _context;
        public CreateUserCommandHandler(
            IUsersRepository usersRepo,
            IDomainService domain,
            IPasswordHasher hasher,
            IEventSender eventSender,
            IContext context
        ) {
            _usersRepo = usersRepo;
            _domain = domain;
            _hasher = hasher;
            _eventSender = eventSender;
            _context = context;
        }

        public void Execute(CreateUserCommand command)
        {
            var creatorId = _context.GetCurrentApplicationUser().Id;

            CheckCommand(command, out UserType userType);

            User user = _domain.CreateUser(
                command.Id,
                command.Name,
                command.Mail,
                _hasher.HashPassord(command.Name, command.Pass),
                command.IsVip,
                userType,
                creatorId
            );

            _usersRepo.Add(user);

            _eventSender.Send(new UserCreated(user.Id, creatorId));
        }

        private void CheckCommand(CreateUserCommand command, out UserType userType)
        {
            if (command == null)
                throw new NullCommandException();

            if (command.Id == null || command.Id == Guid.Empty)
                throw new UserIdRequiredException();

            if (_usersRepo.ById(command.Id) != null)
                throw new UserIdInUseException();

            if (command.Type == null)
                throw new UserTypeRequiredException();

            if (!Enum.TryParse(command.Type, out userType))
                throw new UserTypeBatFormatException();

            if (command.Name == null)
                throw new UserNameRequiredException();

            if (_usersRepo.FindByNameType(command.Name, userType) != null)
                throw new UserNameInUseException(command.Name);

            if (command.Mail == null)
                throw new UserMailRequiredException();

            if (!MailIsCorrect(command.Mail))
                throw new UserMailBatFormatException();

            if (command.Pass == null)
                throw new UserPassRequiredException();

            if (!PassIsCorrect(command.Pass))
                throw new UserPassBatFormatException();
        }

        private bool PassIsCorrect(string pass)
        {
            return true
                && pass != pass.ToLower()
                && pass != pass.ToUpper()
                && pass.Any(letter => IsNumber(letter))
                && pass.Length >= 10;
        }

        private bool MailIsCorrect(string mail)
        {
            int indexOf_ARoba = mail.IndexOf('@'),
                indexOf_Last_Arroba = mail.LastIndexOf('@'),
                indexOf_Dot = mail.LastIndexOf('.');

            return true
                && indexOf_ARoba > 0
                && indexOf_Last_Arroba == indexOf_ARoba
                && indexOf_Dot > indexOf_ARoba + 1 
                && indexOf_Dot < mail.Length - 1

                && !IsNumber(mail[0])
                && !IsNumber(mail[indexOf_ARoba + 1])
                && !IsNumber(mail[indexOf_Dot + 1])

                && mail.ToUpper().All(letter => EmailAllowedCharacters.ToUpper().Any(allowed => allowed == letter));
        }

        private bool IsNumber(char letter)
        {
            return int.TryParse(letter.ToString(), out int notANumber);
        }
    }
}
