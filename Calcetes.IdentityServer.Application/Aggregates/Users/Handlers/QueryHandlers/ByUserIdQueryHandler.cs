﻿using BECore.Application.Automaticals;
using BECore.Application.Disposable;
using BECore.Application.Handlers;
using BECore.Application.Message;
using Calcetes.IdentityServer.Application.Aggregates.Users.Converters;
using Calcetes.IdentityServer.Application.Aggregates.Users.Dtos.Dtos;
using Calcetes.IdentityServer.Application.Aggregates.Users.Dtos.Queries;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Repositories;
using Calcetes.IdentityServer.Domain.Base.Exceptions;
using System;

namespace Calcetes.IdentityServer.Application.Aggregates.Users.Handlers.QueryHandlers
{
    public class ByUserIdQueryHandler : AutoDisposable, IQueryHandler<ByUserIdQuery, QueryResult<UserDto>>, IInjectable
    {
        private readonly IUsersMemory _usersMemo;

        public ByUserIdQueryHandler(
            IUsersMemory usersMemo
        ) {
            _usersMemo = usersMemo;
        }

        public QueryResult<UserDto> Retrieve(ByUserIdQuery query)
        {
            CheckQuery(query);

            var user = _usersMemo.Find(query.UserId);

            if (user == null)
                throw new UserNotFoundException(query.UserId);

            return new QueryResult<UserDto> { Result = UserConverter.ToDto(user) };
        }

        private void CheckQuery(ByUserIdQuery query)
        {
            if (query == null)
                throw new NullQueryException();

            if (query.UserId == Guid.Empty)
                throw new UserIdRequiredException();
        }
    }
}
