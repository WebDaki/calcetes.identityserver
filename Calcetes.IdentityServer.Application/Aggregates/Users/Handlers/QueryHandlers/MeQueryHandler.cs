﻿using BECore.Application.Automaticals;
using BECore.Application.Disposable;
using BECore.Application.Handlers;
using BECore.Application.Message;
using Calcetes.IdentityServer.Application.Aggregates.Users.Converters;
using Calcetes.IdentityServer.Application.Aggregates.Users.Dtos.Dtos;
using Calcetes.IdentityServer.Application.Aggregates.Users.Dtos.Queries;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Repositories;
using Calcetes.IdentityServer.Domain.Aggregates.UserSessions.Repositories;
using Calcetes.IdentityServer.Domain.Services;

namespace Calcetes.IdentityServer.Application.Aggregates.Users.Handlers.QueryHandlers
{
    public class MeQueryHandler : AutoDisposable, IQueryHandler<MeQuery, QueryResult<UserDto>>, IInjectable
    {
        private readonly IUsersMemory _usersMemo;
        private readonly IContext _context;

        public MeQueryHandler(
            IUsersMemory usersMemo,
            IContext context
        )
        {
            _usersMemo = usersMemo;
            _context = context;
        }

        public QueryResult<UserDto> Retrieve(MeQuery query)
        {
            var currentUserId = _context.GetCurrentApplicationUser().Id;

            var user = _usersMemo.Find(currentUserId);

            if (user == null)
                throw new UserNotFoundException(currentUserId);

            return new QueryResult<UserDto> { Result = UserConverter.ToDto(user) };
        }
    }
}
