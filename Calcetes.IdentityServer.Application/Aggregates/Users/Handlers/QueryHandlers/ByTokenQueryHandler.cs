﻿using BECore.Application.Automaticals;
using BECore.Application.Disposable;
using BECore.Application.Handlers;
using BECore.Application.Message;
using Calcetes.IdentityServer.Application.Aggregates.Users.Converters;
using Calcetes.IdentityServer.Application.Aggregates.Users.Dtos.Dtos;
using Calcetes.IdentityServer.Application.Aggregates.Users.Dtos.Queries;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Repositories;
using Calcetes.IdentityServer.Domain.Aggregates.UserSessions.Repositories;
using System;

namespace Calcetes.IdentityServer.Application.Aggregates.Users.Handlers.QueryHandlers
{
    public class ByTokenQueryHandler : AutoDisposable, IQueryHandler<ByTokenQuery, QueryResult<UserDto>>, IInjectable
    {
        private readonly IUserSessionsRepository _sesionsRepo;
        private readonly IUsersRepository _usersRepo;

        public ByTokenQueryHandler(
            IUserSessionsRepository sesionsRepo,
            IUsersRepository usersRepo
        )
        {
            _sesionsRepo = sesionsRepo;
            _usersRepo = usersRepo;
        }

        public QueryResult<UserDto> Retrieve(ByTokenQuery query)
        {
            var sesion = _sesionsRepo.ByToken(query.Token);

            if (sesion == null)
                throw new UnauthorizedAccessException();

            var user = _usersRepo.ById(sesion.UserId);

            if (user == null)
                throw new UnauthorizedAccessException();

            return new QueryResult<UserDto> { Result = UserConverter.ToDto(user) };
        }
    }
}
