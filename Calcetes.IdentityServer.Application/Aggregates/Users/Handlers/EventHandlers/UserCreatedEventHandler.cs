﻿using BECore.Application.Automaticals;
using BECore.Application.Disposable;
using BECore.Application.Handlers;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Events;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Repositories;

namespace Calcetes.IdentityServer.Application.Aggregates.Users.Handlers.EventHandlers
{
    public class UserCreatedEventHandler : AutoDisposable, IEventHandler<UserCreated>, IInjectable
    {
        private readonly IUsersMemory _memory;

        public UserCreatedEventHandler(
            IUsersMemory memory
        ) {
            _memory = memory;
        }

        public void Resolve(UserCreated _event)
        {
            _memory.Find(_event.NewUserId);
        }
    }
}
