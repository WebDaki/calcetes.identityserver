﻿using BECore.Application.Message;

namespace Calcetes.IdentityServer.Application.Aggregates.UserSessions.Dtos.Queries
{
    public class RefreshUserTokenQuery : Query
    {
        public string Token { get; set; }
    }
}
