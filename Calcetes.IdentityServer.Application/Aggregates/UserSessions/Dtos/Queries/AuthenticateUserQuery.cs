﻿using BECore.Application.Message;

namespace Calcetes.IdentityServer.Application.Aggregates.UserSessions.Dtos.Queries
{
    public class AuthenticateUserQuery : Query
    {
        public string Name { get; set; }
        public string Pass { get; set; }
        public string Type { get; set; }
    }
}
