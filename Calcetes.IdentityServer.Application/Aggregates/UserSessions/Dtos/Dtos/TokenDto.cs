﻿namespace Calcetes.IdentityServer.Application.Aggregates.UserSessions.Dtos.Dtos
{
    public class TokenDto
    {
        public string Token { get; set; }
    }
}
