﻿using BECore.Application.Automaticals;
using BECore.Application.Base;
using BECore.Application.Disposable;
using BECore.Application.Handlers;
using BECore.Application.Hashers;
using BECore.Application.Message;
using Calcetes.IdentityServer.Domain.Aggregates.UserSessions.Entities;
using Calcetes.IdentityServer.Domain.Aggregates.UserSessions.Repositories;
using Calcetes.IdentityServer.Domain.Services;
using System;

namespace Calcetes.IdentityServer.Application.Aggregates.UserSessions.Handlers.QueryHandlers
{
    public abstract class BuildSessionQueryHandler<Q, R> : AutoDisposable, IQueryHandler<Q, R>, IInjectable
        where Q : IQuery
        where R : IQueryResult
    {
        private const string SESSION_LIVE_HOURS_CONFIG_KEY = "Sesion:LiveHours";
        private const string SESSION_LIVE_MINUTES_CONFIG_KEY = "Sesion:LiveMinutes";
        private readonly IUserSessionsRepository _sessionsRepo;
        private readonly IHasher _hasher;
        private readonly IUtcNowSevice _utcNow;
        private readonly IDomainService _domain;
        private readonly IConfig _config;

        public BuildSessionQueryHandler(
            IUserSessionsRepository sessionsRepo,
            IHasher hasher,
            IUtcNowSevice utcNow,
            IDomainService domain,
            IConfig config
        ) {
            _sessionsRepo = sessionsRepo;
            _hasher = hasher;
            _utcNow = utcNow;
            _domain = domain;
            _config = config;
        }

        public abstract R Retrieve(Q query);

        public UserSession BuildSession(Guid userId)
        {
            DateTime? validity = GetSesionExpirationDate();

            string token = GetSessionToken(userId, validity);

            var session = _domain.CreateUserSession(userId, token, validity);

            _sessionsRepo.Add(session);

            return session;
        }

        private string GetSessionToken(Guid id, DateTime? validity)
        {
            return _hasher.Hash(id.ToString() + (validity.HasValue ? validity.Value.Ticks.ToString() : string.Empty));
        }

        private DateTime? GetSesionExpirationDate()
        {
            var hoursString = _config.Get(SESSION_LIVE_HOURS_CONFIG_KEY);
            if (!int.TryParse(hoursString, out var hours))
                hours = 0;

            var minutesString = _config.Get(SESSION_LIVE_MINUTES_CONFIG_KEY);
            if (!int.TryParse(minutesString, out var minutes))
                minutes = 0;

            if (hours == 0 && minutes == 0)
                return null;

            return _utcNow.Get().AddHours(hours).AddMinutes(minutes);
        }
    }
}
