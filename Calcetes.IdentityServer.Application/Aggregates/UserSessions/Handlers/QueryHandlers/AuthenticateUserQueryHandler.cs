﻿using BECore.Application.Automaticals;
using BECore.Application.Base;
using BECore.Application.Hashers;
using BECore.Application.Message;
using Calcetes.IdentityServer.Application.Aggregates.UserSessions.Dtos.Dtos;
using Calcetes.IdentityServer.Application.Aggregates.UserSessions.Dtos.Queries;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Repositories;
using Calcetes.IdentityServer.Domain.Aggregates.UserSessions.Repositories;
using Calcetes.IdentityServer.Domain.Base.Exceptions;
using Calcetes.IdentityServer.Domain.Enumerations;
using Calcetes.IdentityServer.Domain.Services;
using System;

namespace Calcetes.IdentityServer.Application.Aggregates.UserSessions.Handlers.QueryHandlers
{
    public class AuthenticateUserQueryHandler : BuildSessionQueryHandler<AuthenticateUserQuery, QueryResult<TokenDto>>, IInjectable
    {
        private readonly IUserSessionsRepository _sessionsRepo;
        private readonly IUsersRepository _usersRepo;
        private readonly IPasswordHasher _passHasher;

        public AuthenticateUserQueryHandler(
            IUserSessionsRepository sessionsRepo,
            IUsersRepository usersRepo,
            IPasswordHasher passHasher,
            IHasher hasher,
            IUtcNowSevice utcNow,
            IDomainService domain,
            IConfig config
        )
            : base (sessionsRepo, hasher, utcNow, domain, config)
        {
            _sessionsRepo = sessionsRepo;
            _usersRepo = usersRepo;
            _passHasher = passHasher;
        }

        public override QueryResult<TokenDto> Retrieve(AuthenticateUserQuery query)
        {
            CheckQuery(query);

            var user = _usersRepo.FindByNameTypePass(
                name: query.Name,
                type: GetUserType(query.Type),
                hashedPass: GetUserPass(query.Name, query.Pass)
            );

            if (user == null)
                throw new UnauthorizedAccessException();

            var session = _sessionsRepo.ByUserId(user.Id);

            if (session == null)
                session = BuildSession(user.Id);

            return new QueryResult<TokenDto> { Result = new TokenDto { Token = session.Token } };
        }

        private void CheckQuery(AuthenticateUserQuery query)
        {
            if (query == null)
                throw new NullQueryException();

            if (query.Name == null)
                throw new UserNameRequiredException();

            if (query.Pass == null)
                throw new UserPassRequiredException();
        }

        private string GetUserPass(string name, string pass)
        {
            return _passHasher.HashPassord(name, pass);
        }

        private UserType GetUserType(string type)
        {
            if (Enum.TryParse<UserType>(type, out var userType))
                return userType;
            throw new NotImplementedException();
        }
    }
}
