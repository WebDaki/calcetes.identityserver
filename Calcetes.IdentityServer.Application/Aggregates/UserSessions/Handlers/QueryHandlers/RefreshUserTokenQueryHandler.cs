﻿using BECore.Application.Automaticals;
using BECore.Application.Base;
using BECore.Application.Hashers;
using BECore.Application.Message;
using Calcetes.IdentityServer.Application.Aggregates.UserSessions.Dtos.Dtos;
using Calcetes.IdentityServer.Application.Aggregates.UserSessions.Dtos.Queries;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions;
using Calcetes.IdentityServer.Domain.Aggregates.UserSessions.Repositories;
using Calcetes.IdentityServer.Domain.Base.Exceptions;
using Calcetes.IdentityServer.Domain.Services;
using System;

namespace Calcetes.IdentityServer.Application.Aggregates.UserSessions.Handlers.QueryHandlers
{
    public class RefreshUserTokenQueryHandler : BuildSessionQueryHandler<RefreshUserTokenQuery, QueryResult<TokenDto>>, IInjectable
    {
        private readonly IUserSessionsRepository _sessionsRepo;

        public RefreshUserTokenQueryHandler(
            IUserSessionsRepository sessionsRepo,
            IHasher hasher,
            IUtcNowSevice utcNow,
            IDomainService domain,
            IConfig config
        )
            : base(sessionsRepo, hasher, utcNow, domain, config)
        {
            _sessionsRepo = sessionsRepo;
        }

        public override QueryResult<TokenDto> Retrieve(RefreshUserTokenQuery query)
        {
            CheckQuery(query);

            var oldSession = _sessionsRepo.ByToken(query.Token, true);

            if (oldSession == null)
                throw new UnauthorizedAccessException();

            var newSession = BuildSession(oldSession.UserId);

            return new QueryResult<TokenDto> { Result = new TokenDto { Token = newSession.Token } };
        }

        private void CheckQuery(RefreshUserTokenQuery query)
        {
            if (query == null)
                throw new NullQueryException();

            if (query.Token == null)
                throw new UserTokenRequiredException();
        }
    }
}
