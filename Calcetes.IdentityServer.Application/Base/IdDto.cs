﻿using BECore.Application.Dtos;
using System;

namespace Calcetes.IdentityServer.Application.Base
{
    public class IdDto : Dto
    {
        public Guid Id { get; set; }
    }
}
