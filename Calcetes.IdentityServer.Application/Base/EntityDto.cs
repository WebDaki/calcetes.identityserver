﻿using System;

namespace Calcetes.IdentityServer.Application.Base
{
    public class EntityDto : IdDto
    {
        public DateTime Created { get; set; }
        public Guid Creator { get; set; }
        public DateTime Updated { get; set; }
        public Guid Updator { get; set; }
    }
}
