﻿using BECore.Application.Automaticals;
using Calcetes.IdentityServer.Domain.Identification;
using Calcetes.IdentityServer.Domain.Services;
using Microsoft.AspNetCore.Http;

namespace Calcetes.IdentityServer.Infrastructure.Contexts
{
    public class HttpContext : IContext, IInjectable
    {
        private readonly IHttpContextAccessor _httpContext;
        public HttpContext(IHttpContextAccessor httpContext)
        {
            _httpContext = httpContext;
        }

        public ContextUser GetCurrentApplicationUser() {
            return (ContextUser)_httpContext.HttpContext.User.Identity;
        }
    }
}
