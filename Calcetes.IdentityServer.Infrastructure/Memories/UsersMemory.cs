﻿using BECore.Application.Base;
using BECore.Infrastructure.Memories;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Entities;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Repositories;

namespace Calcetes.IdentityServer.Infrastructure.Memories
{
    public class UsersMemory : RechargeableMemoryBase<User>, IUsersMemory
    {
        public UsersMemory(IConfig config, ICache cache, IUsersRepository repo) : base(config, cache, repo, repo) { }
    }
}
