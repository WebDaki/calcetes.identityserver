﻿CREATE TABLE User_User (
	Id VARCHAR(38) PRIMARY KEY,

    Name VARCHAR(50),
    Mail VARCHAR(100),
    Pass VARCHAR(64),
	IsVip BIT,
    Type INT NOT NULL,

    Version INT,
    Created DATETIME,
    CreatorId VARCHAR(38) NOT NULL,
    Updated DATETIME,
    UpdatorId VARCHAR(38) NOT NULL
);