﻿CREATE TABLE UserSession_UserSession (
	Id VARCHAR(38) PRIMARY KEY,

    Token VARCHAR(64),
    UserId VARCHAR(38),
	Validity DATETIME,

    Version INT,
    Created DATETIME,
    CreatorId VARCHAR(38) NOT NULL,
    Updated DATETIME,
    UpdatorId VARCHAR(38) NOT NULL
);