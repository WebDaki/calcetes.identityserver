﻿using BECore.Application.Automaticals;
using BECore.Infrastructure.Events;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Calcetes.IdentityServer.Infrastructure.HealthChecks
{
    public class EventsHealthCheck : IHealthCheck, IInjectable
    {
        private readonly IEventTester _events;
        public EventsHealthCheck(
            IEventTester events
        ) {
            _events = events;
        }

        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                _events.TestConnection();
                return Task.FromResult(
                    HealthCheckResult.Healthy("Connection stablished."));
            }
            catch (Exception e)
            {
                return Task.FromResult(
                    HealthCheckResult.Unhealthy("Impossible to stablish connection.", e));
            }
            finally
            {
                if (_events is IDisposable disposable)
                    disposable.Dispose();
            }
        }
    }
}
