﻿using BECore.Application.Automaticals;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Repositories;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Calcetes.IdentityServer.Infrastructure.HealthChecks
{
    public class MySQLHealthCheck : IHealthCheck, IInjectable
    {
        private readonly IUsersRepository _usersRepo;
        public MySQLHealthCheck(
            IUsersRepository usersRepo
        ) {
            _usersRepo = usersRepo;
        }

        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                _usersRepo.ById(Guid.NewGuid());
                return Task.FromResult(
                    HealthCheckResult.Healthy("Connection stablished."));
            }
            catch (Exception e)
            {
                return Task.FromResult(
                    HealthCheckResult.Unhealthy("Impossible to stablish connection.", e));
            }
            finally
            {
                if (_usersRepo is IDisposable disposable)
                    disposable.Dispose();
            }
        }
    }
}
