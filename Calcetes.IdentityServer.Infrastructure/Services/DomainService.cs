﻿using BECore.Application.Automaticals;
using BECore.Domain;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Entities;
using Calcetes.IdentityServer.Domain.Aggregates.UserSessions.Entities;
using Calcetes.IdentityServer.Domain.Enumerations;
using Calcetes.IdentityServer.Domain.Services;
using System;

namespace Calcetes.IdentityServer.Infrastructure.Services
{
    public class DomainService : IDomainService, IInjectable
    {
        private T Create<T>(Guid id, Guid creatorId) where T : AggregateRoot
        {
            var item = Activator.CreateInstance<T>();
            item.Id = id;
            item.Created = DateTime.UtcNow;
            item.CreatorId = creatorId;
            item.Updated = item.Created;
            item.UpdatorId = creatorId;
            return item;
        }

        public User CreateUser(Guid id, string name, string mail, string pass, bool isVip, UserType type, Guid creatorId)
        {
            User user = Create<User>(id, creatorId);
            user.Name = name;
            user.Mail = mail;
            user.Pass = pass;
            user.IsVip = isVip;
            user.Type = type;
            return user;
        }

        public UserSession CreateUserSession(Guid userId, string token, DateTime? validity)
        {
            UserSession session = Create<UserSession>(Guid.NewGuid(), userId);
            session.UserId = userId;
            session.Token = token;
            session.Validity = validity;
            return session;
        }
    }
}
