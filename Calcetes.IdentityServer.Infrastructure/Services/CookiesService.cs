﻿using BECore.Application.Automaticals;
using Calcetes.IdentityServer.Domain.Services;
using Microsoft.AspNetCore.Http;
using System.Linq;

namespace Calcetes.IdentityServer.Infrastructure.Services
{
    public class CookiesService : ICookiesService, IInjectable
    {
        private readonly IHttpContextAccessor _httpContext;
        public CookiesService(IHttpContextAccessor httpContext)
        {
            _httpContext = httpContext;
        }

        public string Get(string key)
        {
            if (_httpContext.HttpContext.Request.Headers.TryGetValue(key, out var value))
                return value.FirstOrDefault();

            return null;
        }

        public void Set(string key, string value)
        {
            if (_httpContext.HttpContext.Response.Headers.ContainsKey(key))
                _httpContext.HttpContext.Response.Headers.Remove(key);

            _httpContext.HttpContext.Response.Headers.Add(key, value);
        }
    }
}
