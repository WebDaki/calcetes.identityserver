﻿using BECore.Application.Automaticals;
using Calcetes.IdentityServer.Domain.Services;
using System;

namespace Calcetes.IdentityServer.Infrastructure.Services
{
    public class UtcNowSevice : IUtcNowSevice, IInjectable
    {
        public DateTime Get()
        {
            return DateTime.UtcNow;
        }
    }
}
