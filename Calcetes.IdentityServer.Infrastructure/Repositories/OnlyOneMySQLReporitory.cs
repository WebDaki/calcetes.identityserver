﻿using BECore.Application.Base;
using BECore.Domain;
using BECore.Infrastructure.Repositories;

namespace Calcetes.IdentityServer.Infrastructure.Repositories
{
    public abstract class OnlyOneMySQLReporitory<T> : MySqlDapperRepositoryBase<T> where T : IAggregateRoot
    {
        private const string DDBBConnectionString = "DDBB:ConnectionString";

        public OnlyOneMySQLReporitory(IConfig config) : base(config, DDBBConnectionString) { }
    }
}
