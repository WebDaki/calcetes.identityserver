﻿using BECore.Application.Automaticals;
using BECore.Application.Base;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Entities;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Repositories;
using Calcetes.IdentityServer.Domain.Enumerations;
using System.Collections.Generic;
using System.Linq;

namespace Calcetes.IdentityServer.Infrastructure.Repositories
{
    public class UsersRepository : OnlyOneMySQLReporitory<User>, IUsersRepository, IInjectable
    {
        public UsersRepository(IConfig config) : base(config) { }

        public User FindByNameTypePass(string name, UserType type, string hashedPass)
        {
            return GetByCondition<User>($"Name = '{ name }' AND Type = { (int)type } AND Pass = '{ hashedPass }'").FirstOrDefault();
        }

        public User FindByNameType(string name, UserType type)
        {
            return GetByCondition<User>($"Name = '{ name }' AND Type = { (int)type }").FirstOrDefault();
        }

        public override List<User> SetChilds(List<User> items)
        {
            return items;
        }
    }
}
