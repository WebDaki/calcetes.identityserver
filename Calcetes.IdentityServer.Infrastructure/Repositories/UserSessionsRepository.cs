﻿using BECore.Application.Automaticals;
using BECore.Application.Base;
using Calcetes.IdentityServer.Domain.Aggregates.UserSessions.Entities;
using Calcetes.IdentityServer.Domain.Aggregates.UserSessions.Repositories;
using Calcetes.IdentityServer.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Calcetes.IdentityServer.Infrastructure.Repositories
{
    public class UserSessionsRepository : OnlyOneMySQLReporitory<UserSession>, IUserSessionsRepository, IInjectable
    {
        private readonly IUtcNowSevice _utcNow;

        public UserSessionsRepository(
            IConfig config,
            IUtcNowSevice utcNow
        )
            : base(config)
        {
            _utcNow = utcNow;
        }

        public UserSession ByToken(string token, bool avoidValidity = false)
        {
            return GetByCondition<UserSession>($"Token = '{ token }'{ ( avoidValidity ? string.Empty : $" AND { IsValid() }" ) }").FirstOrDefault();
        }

        public UserSession ByUserId(Guid userId)
        {
            return GetByCondition<UserSession>($"UserId = '{ userId }' AND { IsValid() }").FirstOrDefault();
        }

        private string IsValid()
        {
            return $"(Validity IS NULL OR Validity > '{ _utcNow.Get().ToString("yyyy-MM-dd HH:mm:ss") }')";
        }

        public override List<UserSession> SetChilds(List<UserSession> items)
        {
            return items;
        }
    }
}
