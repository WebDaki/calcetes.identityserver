﻿using BECore.Domain.Exceptions;

namespace Calcetes.IdentityServer.Domain.Base.Exceptions
{
    public class NullCommandException : DomainException
    {
        public NullCommandException() : base("Command is required") { }
    }
}
