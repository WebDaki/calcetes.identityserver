﻿using BECore.Domain.Exceptions;

namespace Calcetes.IdentityServer.Domain.Base.Exceptions
{
    public class NullQueryException : DomainException
    {
        public NullQueryException() : base("Query is required") { }
    }
}
