﻿namespace Calcetes.IdentityServer.Domain.Enumerations
{
    public enum UserType
    {
        Undefined = 0,
        User = 1,
        Admin = 2,
        Resource = 3
    }
}
