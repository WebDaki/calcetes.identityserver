﻿namespace Calcetes.IdentityServer.Domain.Services
{
    public interface ICookiesService
    {
        string Get(string key);
        void Set(string key, string value);
    }
}