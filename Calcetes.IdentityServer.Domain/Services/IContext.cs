﻿using Calcetes.IdentityServer.Domain.Identification;

namespace Calcetes.IdentityServer.Domain.Services
{
    public interface IContext
    {
        ContextUser GetCurrentApplicationUser();
    }
}
