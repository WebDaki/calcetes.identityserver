﻿using Calcetes.IdentityServer.Domain.Aggregates.Users.Entities;
using Calcetes.IdentityServer.Domain.Aggregates.UserSessions.Entities;
using Calcetes.IdentityServer.Domain.Enumerations;
using System;

namespace Calcetes.IdentityServer.Domain.Services
{
    public interface IDomainService
    {
        User CreateUser(Guid id, string name, string mail, string pass, bool isVip, UserType type, Guid creatorId);
        UserSession CreateUserSession(Guid userId, string token, DateTime? validity);
    }
}
