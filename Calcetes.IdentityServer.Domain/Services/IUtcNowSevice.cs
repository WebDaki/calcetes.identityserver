﻿using System;

namespace Calcetes.IdentityServer.Domain.Services
{
    public interface IUtcNowSevice
    {
        DateTime Get();
    }
}