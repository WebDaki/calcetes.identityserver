﻿using System;
using System.Security.Principal;

namespace Calcetes.IdentityServer.Domain.Identification
{
    public class ContextUser : IIdentity
    {
        private readonly Guid _id;
        private readonly string _name = null;

        public string AuthenticationType => "token";
        public bool IsAuthenticated { get { return string.IsNullOrEmpty(Name); } }
        public string Name { get { return _name; } }
        public Guid Id { get { return _id; } }

        public ContextUser(Guid userId, string userName)
        {
            _id = userId;
            _name = userName;
        }
    }
}
