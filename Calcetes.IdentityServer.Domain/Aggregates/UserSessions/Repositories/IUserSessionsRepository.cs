﻿using BECore.Domain.Repositories;
using Calcetes.IdentityServer.Domain.Aggregates.UserSessions.Entities;
using System;

namespace Calcetes.IdentityServer.Domain.Aggregates.UserSessions.Repositories
{
    public interface IUserSessionsRepository : IRepositoryBase<UserSession>
    {
        UserSession ByToken(string token, bool avoidValidity = false);
        UserSession ByUserId(Guid userId);
    }
}
