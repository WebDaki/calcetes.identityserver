﻿using BECore.Domain;
using System;

namespace Calcetes.IdentityServer.Domain.Aggregates.UserSessions.Entities
{
    public class UserSession : AggregateRoot
    {
        public string Token { get; set; }
        public Guid UserId { get; set; }
        public DateTime? Validity { get; set; }
    }
}
