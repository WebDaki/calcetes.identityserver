﻿using BECore.Domain;
using System;

namespace Calcetes.IdentityServer.Domain.Aggregates.Users.Events
{
    public class UserCreated : Event
    {
        public Guid NewUserId { get; set; }
        public UserCreated(Guid newUserId, Guid creatorId) : base(creatorId) {
            NewUserId = newUserId;
        }
    }
}
