﻿using BECore.Domain.Memories;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Entities;

namespace Calcetes.IdentityServer.Domain.Aggregates.Users.Repositories
{
    public interface IUsersMemory : IMemoryBase<User> {}
}
