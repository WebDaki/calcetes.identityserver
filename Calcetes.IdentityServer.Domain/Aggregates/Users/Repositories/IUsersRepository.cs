﻿using BECore.Domain.Repositories;
using Calcetes.IdentityServer.Domain.Aggregates.Users.Entities;
using Calcetes.IdentityServer.Domain.Enumerations;

namespace Calcetes.IdentityServer.Domain.Aggregates.Users.Repositories
{
    public interface IUsersRepository : IRepositoryBase<User>
    {
        User FindByNameTypePass(string name, UserType type, string hashedPass);
        User FindByNameType(string name, UserType type);
    }
}
