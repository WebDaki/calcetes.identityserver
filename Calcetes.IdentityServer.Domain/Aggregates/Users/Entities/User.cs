﻿using BECore.Domain;
using Calcetes.IdentityServer.Domain.Enumerations;

namespace Calcetes.IdentityServer.Domain.Aggregates.Users.Entities
{
    public class User : AggregateRoot
    {
        public string Name { get; set; }
        public string Mail { get; set; }
        public string Pass { get; set; }
        public bool IsVip { get; set; }
        public UserType Type { get; set; }
    }
}
