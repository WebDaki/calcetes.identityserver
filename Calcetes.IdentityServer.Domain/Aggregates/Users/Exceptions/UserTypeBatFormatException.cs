﻿using BECore.Domain.Exceptions;
using Calcetes.IdentityServer.Domain.Enumerations;
using System;
using System.Linq;

namespace Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions
{
    public class UserTypeBatFormatException : DomainException
    {
        public UserTypeBatFormatException() : base($"Type must have one of this values: { string.Join(", ", Enum.GetValues(typeof(UserType)).Cast<UserType>().Select(x => $"'{ x.ToString() }'")) }") { }
    }
}
