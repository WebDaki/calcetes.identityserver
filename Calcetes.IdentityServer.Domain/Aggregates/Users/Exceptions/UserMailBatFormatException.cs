﻿using BECore.Domain.Exceptions;

namespace Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions
{
    public class UserMailBatFormatException : DomainException
    {
        public UserMailBatFormatException() : base("Mail must be a valid email address") { }
    }
}
