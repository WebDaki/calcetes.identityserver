﻿using BECore.Domain.Exceptions;

namespace Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions
{
    public class UserPassBatFormatException : DomainException
    {
        public UserPassBatFormatException() : base($"Pass must contain a lower, an upper letter, a number, and almost 10 characters") { }
    }
}
