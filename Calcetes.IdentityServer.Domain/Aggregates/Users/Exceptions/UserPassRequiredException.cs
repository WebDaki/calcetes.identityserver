﻿using BECore.Domain.Exceptions;

namespace Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions
{
    public class UserPassRequiredException : DomainException
    {
        public UserPassRequiredException() : base("Pass is required") { }
    }
}
