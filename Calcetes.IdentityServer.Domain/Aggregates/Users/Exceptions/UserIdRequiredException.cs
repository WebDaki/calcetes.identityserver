﻿using BECore.Domain.Exceptions;

namespace Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions
{
    public class UserIdRequiredException : DomainException
    {
        public UserIdRequiredException() : base("Id is required") { }
    }
}
