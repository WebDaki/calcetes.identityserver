﻿using BECore.Domain.Exceptions;

namespace Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions
{
    public class UserNameRequiredException : DomainException
    {
        public UserNameRequiredException() : base("Name is required") { }
    }
}
