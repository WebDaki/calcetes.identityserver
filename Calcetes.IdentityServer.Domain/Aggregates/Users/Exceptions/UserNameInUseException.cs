﻿using BECore.Domain.Exceptions;

namespace Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions
{
    public class UserNameInUseException : DomainException
    {
        public UserNameInUseException(string userName) : base($"Name '{ userName }' is in use") { }
    }
}
