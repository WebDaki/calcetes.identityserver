﻿using BECore.Domain.Exceptions;

namespace Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions
{
    public class UserIdInUseException : DomainException
    {
        public UserIdInUseException() : base("Id is used by another user") { }
    }
}
