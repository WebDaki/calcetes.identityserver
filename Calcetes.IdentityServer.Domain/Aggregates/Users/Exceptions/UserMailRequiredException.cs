﻿using BECore.Domain.Exceptions;

namespace Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions
{
    public class UserMailRequiredException : DomainException
    {
        public UserMailRequiredException() : base("Mail is required") { }
    }
}
