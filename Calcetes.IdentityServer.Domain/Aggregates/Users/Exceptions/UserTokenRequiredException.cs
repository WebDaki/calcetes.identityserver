﻿using BECore.Domain.Exceptions;

namespace Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions
{
    public class UserTokenRequiredException : DomainException
    {
        public UserTokenRequiredException() : base("Token is required") { }
    }
}
