﻿using BECore.Domain.Exceptions;
using System;

namespace Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions
{
    public class UserNotFoundException : DomainException
    {
        public UserNotFoundException(Guid userId) : base($"User with Id '{ userId }' not exists") { }
    }
}
