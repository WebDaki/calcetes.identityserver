﻿using BECore.Domain.Exceptions;

namespace Calcetes.IdentityServer.Domain.Aggregates.Users.Exceptions
{
    public class UserTypeRequiredException : DomainException
    {
        public UserTypeRequiredException() : base("Type is required") { }
    }
}
